#!/bin/bash

PACKAGE="$1"

if [ -z "${PACKAGE}" ]; then
    echo "Usage: $0 PACKAGE_NAME"
    exit 1
fi

BASEDIR="/srv/builder/incoming/packages"
ARCHLIST="amd64 arm64 armhf riscv64 source"

if [ ! -d "${BASEDIR}/${PACKAGE}" ]; then
    echo "ERROR: package '$PACKAGE' not found!"
    exit 1
fi

quit () {
    # shellcheck disable=SC2115
    rm -rf "${BASEDIR}/${PACKAGE}"
    # shellcheck disable=SC2086
    exit $1
}

echo "Deploying ${PACKAGE}..."

for arch in ${ARCHLIST}; do
    [ -d "${BASEDIR}/${PACKAGE}/${arch}" ] || continue

    # shellcheck disable=SC2086
    VERSION="$(cat ${BASEDIR}/${PACKAGE}/${arch}/version)"
    # shellcheck disable=SC2086
    # This drops the epoch part from the version number (if any) as it's not part of the filename
    FILEVERSION="$(sed 's/^.*://' ${BASEDIR}/${PACKAGE}/${arch}/version)"
    CHANGES="${BASEDIR}/${PACKAGE}/${arch}/${PACKAGE}_${FILEVERSION}_${arch}.changes"

    if [ -f "${CHANGES}" ]; then
        # Import packages
        # shellcheck disable=SC2086
        DISTRO="$(grep '^Distribution:' ${CHANGES} | sed 's/^.*:[[:space:]]*//')"
        if [ "${DISTRO}" = "unstable" ]; then
            DEST_DIST="staging"
        else
            DEST_DIST="$DISTRO-updates"
        fi
        if grep "non-free-firmware/" "${CHANGES}"; then
            if [ "${DISTRO}" = "bookworm" ]; then
                COMPONENTS="non-free non-free-firmware"
            else
                COMPONENTS="non-free-firmware"
            fi
        else
            COMPONENTS="main"
        fi

        # shellcheck disable=SC2068
        for COMPONENT in ${COMPONENTS}; do
            echo "Deploying ${PACKAGE}:${arch}/${VERSION} into ${DEST_DIST}-${COMPONENT}..."

            aptly repo include -no-remove-files -accept-unsigned -ignore-signatures \
                -repo="mobian-${DEST_DIST}-${COMPONENT}" "${CHANGES}" || quit 1
            # Remove previous versions of packages built from same source package
            aptly repo remove "mobian-${DEST_DIST}-${COMPONENT}" "\$Source (${PACKAGE}), \$Version (<< ${VERSION})" || quit 1
            # Remove previous versions of source package
            aptly repo remove "mobian-${DEST_DIST}-${COMPONENT}" "Name (${PACKAGE}), \$Version (<< ${VERSION})" || quit 1
        done
    else
        echo "No package found for ${PACKAGE}:${arch}/${VERSION}, skipping..."
    fi
done

aptly publish update "${DEST_DIST}" filesystem:mobian:

echo "Updating package listing..."
if [ -f "/var/www/packages/index.html" ]; then
    /opt/ci/bin/listpkg "/var/www/packages/index.html"
fi

quit 0
