package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"sort"
	"strings"

	"github.com/aptly-dev/aptly/database"
	"github.com/aptly-dev/aptly/database/goleveldb"
	"github.com/aptly-dev/aptly/deb"
	aptlyhttp "github.com/aptly-dev/aptly/http"
	"github.com/aptly-dev/aptly/pgp"
	"github.com/aptly-dev/aptly/query"
)

type mobianStablePkg struct {
	name      string
	dBookworm string
	dUpdates  string
	dProposed string
	dSecurity string
	mBookworm string
	mUpdates  string
}

type mobianPkg struct {
	name          string
	dTesting      string
	dUnstable     string
	mTrixie       string
	mStaging      string
	upstream      string
}

var upstreamJSON map[string]string

func buildRepo(name, url, dist string, component string, db database.Storage) (*deb.PackageList, error) {
	var comp []string
	comp = append(comp, component)

	mobianrr, err := deb.NewRemoteRepo(name, url, dist, comp, []string{}, true, false, false)
	if err != nil {
		return nil, err
	}
	dl := aptlyhttp.NewDownloader(0, 1, nil)
	finder := pgp.GPG2Finder()
	verifier := pgp.NewGpgVerifier(finder)
	verifier.AddKeyring("/usr/share/keyrings/debian-archive-keyring.gpg")
	err = verifier.InitKeyring()
	if err != nil {
		return nil, err
	}
	mobianCollectionFactory := deb.NewCollectionFactory(db)
	err = mobianrr.DownloadPackageIndexes(nil, dl, verifier, mobianCollectionFactory, true)
	if err != nil {
		return nil, err
	}
	err = mobianrr.FinalizeDownload(mobianCollectionFactory, nil)
	if err != nil {
		return nil, err
	}

	mobianRefList := mobianrr.RefList()
	mobianPkgCollection := deb.NewPackageCollection(db)
	pkgList, err := deb.NewPackageListFromRefList(mobianRefList, mobianPkgCollection, nil)
	if err != nil {
		return nil, err
	}
	pkgList.PrepareIndex()
	return pkgList, nil
}

func hasPkg(list []mobianPkg, name string) bool {
	for _, v := range list {
		if strings.Compare(v.name, name) == 0 {
			return true
		}
	}
	return false
}

func hasStablePkg(list []mobianStablePkg, name string) bool {
	for _, v := range list {
		if strings.Compare(v.name, name) == 0 {
			return true
		}
	}
	return false
}

func searchRepo(q string, pkgList *deb.PackageList) (*deb.PackageList, error) {
	qry, err := query.Parse(q) //"Name (% gnome-*)")
	if err != nil {
		return nil, err
	}
	res := pkgList.Scan(qry)
	return res, nil
}

func getDebianVersion(list *deb.PackageList, name string) string {
	q := fmt.Sprintf("Name (= %s)", name)
	ret := "-"
	res, err := searchRepo(q, list)
	if err != nil {
		return "ERROR"
	}
	if res.Len() > 0 {
		_ = res.ForEach(func(debianpkg *deb.Package) error {
			ret = debianpkg.Version
			return nil
		})
	}
	return ret
}

func getUpstreamVersion(name string) string {
	version, found := upstreamJSON[name]
	if found {
		return version
	}

	return "-"
}

func printListToFile(fd *os.File, list []mobianPkg) {
	for _, pkg := range list {
		var pkgName string
		var cssClass string

		lastDebVersion := "-"
		if strings.Compare(pkg.dUnstable, "-") != 0 {
			lastDebVersion = pkg.dUnstable
		} else if strings.Compare(pkg.dTesting, "-") != 0 {
			lastDebVersion = pkg.dTesting
		}

		lagBehindDebian := false
		lagBehindStaging := false
		lagBehindUpstream := false

		splitDebVersion := strings.Split(pkg.mStaging, "-")

		if strings.Compare(lastDebVersion, "-") != 0 && deb.CompareVersions(pkg.mStaging, lastDebVersion) < 0 {
			if strings.Contains(pkg.mStaging, "~mobian") {
				splitMobianVersion := strings.Split(pkg.mStaging, "~mobian")
				if deb.CompareVersions(splitMobianVersion[0], lastDebVersion) < 0 {
					lagBehindDebian = true
				}
			} else {
				lagBehindDebian = true
			}
		}
		if deb.CompareVersions(pkg.mTrixie, pkg.mStaging) < 0 {
			lagBehindStaging = true
		}
		if deb.CompareVersions(splitDebVersion[0], pkg.upstream) < 0 {
			lagBehindUpstream = true
		}

		if lagBehindDebian {
			cssClass = " class=\"red\""
		} else if lagBehindStaging {
			cssClass = " class=\"org\""
		} else {
			cssClass = ""
		}

		if strings.Compare(lastDebVersion, "-") != 0 {
			pkgName = fmt.Sprintf("<a href=\"https://tracker.debian.org/pkg/%s\"%s>%s</a>", pkg.name, cssClass, pkg.name)
		} else {
			pkgName = pkg.name
		}

		fmt.Fprintf(fd, "        <tr>\n")
		fmt.Fprintf(fd, "            <td%s>%s</td>\n", cssClass, pkgName)
		fmt.Fprintf(fd, "            <td>%s</td>\n", pkg.dTesting)
		fmt.Fprintf(fd, "            <td>%s</td>\n", pkg.dUnstable)
		if lagBehindStaging {
			fmt.Fprintf(fd, "            <td class=\"prpl\">%s</td>\n", pkg.mTrixie)
		} else {
			fmt.Fprintf(fd, "            <td>%s</td>\n", pkg.mTrixie)
		}
		if lagBehindDebian {
			fmt.Fprintf(fd, "            <td class=\"prpl\">%s</td>\n", pkg.mStaging)
		} else {
			fmt.Fprintf(fd, "            <td>%s</td>\n", pkg.mStaging)
		}
		if lagBehindUpstream {
			fmt.Fprintf(fd, "            <td class=\"prpl\">%s</td>\n", pkg.upstream)
		} else {
			fmt.Fprintf(fd, "            <td>%s</td>\n", pkg.upstream)
		}
		fmt.Fprintf(fd, "        </tr>\n")
	}
}

func printStableListToFile(fd *os.File, list []mobianStablePkg) {
	for _, pkg := range list {
		var pkgName string
		var cssClass string

		lastDebVersion := "-"
		if strings.Compare(pkg.dProposed, "-") != 0 {
			lastDebVersion = pkg.dProposed
		} else if strings.Compare(pkg.dUpdates, "-") != 0 {
			lastDebVersion = pkg.dUpdates
		} else if strings.Compare(pkg.dBookworm, "-") != 0 {
			lastDebVersion = pkg.dBookworm
		}

		lagBehindDebian := false
		lagBehindUpdates := false

		lastMobianVersion := "-"
		if strings.Compare(pkg.mUpdates, "-") != 0 {
			lastMobianVersion = pkg.mUpdates
		} else if strings.Compare(pkg.mBookworm, "-") != 0 {
			lastMobianVersion = pkg.mBookworm
		}

		if strings.Compare(lastDebVersion, "-") != 0 && deb.CompareVersions(lastMobianVersion, lastDebVersion) < 0 {
			if strings.Contains(pkg.mUpdates, "~mobian") {
				splitMobianVersion := strings.Split(pkg.mUpdates, "~mobian")
				if deb.CompareVersions(splitMobianVersion[0], lastDebVersion) < 0 {
					lagBehindDebian = true
				}
			} else {
				lagBehindDebian = true
			}
		}
		if deb.CompareVersions(pkg.mBookworm, pkg.mUpdates) < 0 {
			lagBehindUpdates = true
		}

		if lagBehindDebian {
			cssClass = " class=\"red\""
		} else if lagBehindUpdates {
			cssClass = " class=\"org\""
		} else {
			cssClass = ""
		}

		if strings.Compare(lastDebVersion, "-") != 0 {
			pkgName = fmt.Sprintf("<a href=\"https://tracker.debian.org/pkg/%s\"%s>%s</a>", pkg.name, cssClass, pkg.name)
		} else {
			pkgName = pkg.name
		}

		fmt.Fprintf(fd, "        <tr>\n")
		fmt.Fprintf(fd, "            <td%s>%s</td>\n", cssClass, pkgName)
		fmt.Fprintf(fd, "            <td>%s</td>\n", pkg.dBookworm)
		fmt.Fprintf(fd, "            <td>%s</td>\n", pkg.dUpdates)
		fmt.Fprintf(fd, "            <td>%s</td>\n", pkg.dProposed)
		fmt.Fprintf(fd, "            <td>%s</td>\n", pkg.dSecurity)
		if lagBehindUpdates {
			fmt.Fprintf(fd, "            <td class=\"prpl\">%s</td>\n", pkg.mBookworm)
		} else {
			fmt.Fprintf(fd, "            <td>%s</td>\n", pkg.mBookworm)
		}
		if lagBehindDebian {
			fmt.Fprintf(fd, "            <td class=\"prpl\">%s</td>\n", pkg.mUpdates)
		} else {
			fmt.Fprintf(fd, "            <td>%s</td>\n", pkg.mUpdates)
		}
		fmt.Fprintf(fd, "        </tr>\n")
	}
}

func createFile(path string, mainList, nonFreeList []mobianPkg, mainStableList, nonFreeStableList []mobianStablePkg) {
	fd, err := os.OpenFile(path, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println("Unable to create file", path)
		os.Exit(1)
	}

	fmt.Fprintf(fd, `
<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            font-family: arial, sans-serif;
            font-size: 11pt;
            border-collapse: collapse;
            table-layout: fixed;
        }
        td, th {
            border: 1px solid #000;
            text-align: left;
            padding: 8px;
        }
        th {
            text-align: center;
        }
        tr:nth-child(even) {
            background-color: #ddd;
        }
        a {
            color: #000;
        }
        .prpl, a.prpl {
            color: #b0b;
        }
        .red, a.red {
            color: #b00;
        }
        .org, a.org {
            color: #770;
        }
    </style>
</head>
<body>
    <h2 id="development">Mobian packages</h2>
    <span class="org">orange</span>&nbsp;-&nbsp;Version in Mobian stable is older than version in Mobian staging<br/>
    <span class="red">red</span>&nbsp;-&nbsp;Version in Mobian staging is older than most recent version in Debian<br/>
    <br/>
    (<a href="#stable">Scroll down</a> for stable packages list)<br/>
    <br/>
    <table>
        <tr>
            <th rowspan="2" colspan="1">Name</th>
            <th rowspan="1" colspan="2">Debian version</th>
            <th rowspan="1" colspan="2">Mobian version</th>
            <th rowspan="2" colspan="1">Upstream version</th>
        </tr>
        <tr>
            <th rowspan="1" colspan="1">testing</th>
            <th rowspan="1" colspan="1">unstable</th>
            <th rowspan="1" colspan="1">trixie</th>
            <th rowspan="1" colspan="1">staging</th>
        </tr>
        <tr>
            <td colspan="6" style="font-size: 12pt; text-align: center"><b>main packages (%d)</b></td>
        </tr>
    `, len(mainList))

	printListToFile(fd, mainList)

	fmt.Fprintf(fd, `
        <tr>
            <td colspan="6" style="font-size: 12pt; text-align: center"><b>non-free-firmware packages (%d)</b></td>
        </tr>
    `, len(nonFreeList))

	printListToFile(fd, nonFreeList)

	fmt.Fprintf(fd, `
    </table><br/>
    <h2 id="stable">Stable packages</h2>
    <span class="org">orange</span>&nbsp;-&nbsp;Version in Mobian bookworm is older than version in Mobian bookworm-updates<br/>
    <span class="red">red</span>&nbsp;-&nbsp;Version in Mobian bookworm(-updates) is older than most recent version in Debian<br/>
    <br/>
    (<a href="#development">Scroll up</a> for development/testing packages list)<br/>
    <br/>
    <table>
        <tr>
            <th rowspan="2" colspan="1">Name</th>
            <th rowspan="1" colspan="4">Debian version</th>
            <th rowspan="1" colspan="2">Mobian version</th>
        </tr>
        <tr>
            <th rowspan="1" colspan="1">bookworm</th>
            <th rowspan="1" colspan="1">updates</th>
            <th rowspan="1" colspan="1">proposed</th>
            <th rowspan="1" colspan="1">securiy</th>
            <th rowspan="1" colspan="1">bookworm</th>
            <th rowspan="1" colspan="1">updates</th>
        </tr>
        <tr>
            <td colspan="7" style="font-size: 12pt; text-align: center"><b>main packages (%d)</b></td>
        </tr>
    `, len(mainStableList))

	printStableListToFile(fd, mainStableList)

	fmt.Fprintf(fd, `
        <tr>
            <td colspan="7" style="font-size: 12pt; text-align: center"><b>non-free-firmware packages (%d)</b></td>
        </tr>
    `, len(nonFreeStableList))

	printStableListToFile(fd, nonFreeStableList)

	fmt.Fprintf(fd, `
    </table>
</body>
</html>
`)
}

func populateMasterList(component string, db database.Storage) ([]mobianStablePkg, []mobianPkg, error) {
	var stableList []mobianStablePkg
	var masterList []mobianPkg

	// Populate list of stable packages

	mobianBkwPkgList, err := buildRepo("mobian", "https://repo.mobian.org", "bookworm", component, db)
	if err != nil {
		return nil, nil, err
	}

	mobianBkwPkgList.ForEach(func(pkg *deb.Package) error {
		if !hasStablePkg(stableList, pkg.Name) {
			newPkg := mobianStablePkg{
				name:      pkg.Name,
				mBookworm: pkg.Version,
				mUpdates:  "-",
			}
			stableList = append(stableList, newPkg)
		}
		return nil
	})

	mobianUpdPkgList, err := buildRepo("mobian", "https://repo.mobian.org", "bookworm-updates", component, db)
	if err != nil {
		return nil, nil, err
	}

	mobianUpdPkgList.ForEach(func(pkg *deb.Package) error {
		if !hasStablePkg(stableList, pkg.Name) {
			newPkg := mobianStablePkg{
				name:      pkg.Name,
				mBookworm: "-",
				mUpdates:  pkg.Version,
			}
			stableList = append(stableList, newPkg)
		} else {
			for i, elem := range stableList {
				if strings.Compare(elem.name, pkg.Name) == 0 {
					myPkg := &stableList[i]
					myPkg.mUpdates = pkg.Version
					break
				}
			}
		}
		return nil
	})

	debianBkwPkgList, err := buildRepo("debian", "https://deb.debian.org/debian", "bookworm", component, db)
	if err != nil {
		return nil, nil, err
	}

	debianUpdPkgList, err := buildRepo("debian", "https://deb.debian.org/debian", "bookworm-updates", component, db)
	if err != nil {
		return nil, nil, err
	}

	debianPruPkgList, err := buildRepo("debian", "https://deb.debian.org/debian", "bookworm-proposed-updates", component, db)
	if err != nil {
		return nil, nil, err
	}

	debianSecPkgList, err := buildRepo("debian", "https://deb.debian.org/debian-security", "bookworm-security", component, db)
	if err != nil {
		return nil, nil, err
	}

	for i, elem := range stableList {
		myPkg := &stableList[i]
		myPkg.dBookworm = getDebianVersion(debianBkwPkgList, elem.name)
		myPkg.dUpdates = getDebianVersion(debianUpdPkgList, elem.name)
		myPkg.dProposed = getDebianVersion(debianPruPkgList, elem.name)
		myPkg.dSecurity = getDebianVersion(debianSecPkgList, elem.name)
	}

	sort.Slice(stableList, func(i, j int) bool {
		return strings.Compare(stableList[i].name, stableList[j].name) < 0
	})

	// Populate list of testing/unstable packages

	mobianTrxPkgList, err := buildRepo("mobian", "https://repo.mobian.org", "trixie", component, db)
	if err != nil {
		return nil, nil, err
	}

	mobianTrxPkgList.ForEach(func(pkg *deb.Package) error {
		if !hasPkg(masterList, pkg.Name) {
			newPkg := mobianPkg{
				name:     pkg.Name,
				mTrixie:  pkg.Version,
				mStaging: "-",
			}
			masterList = append(masterList, newPkg)
		}
		return nil
	})

	mobianStgPkgList, err := buildRepo("mobian", "https://repo.mobian.org", "staging", component, db)
	if err != nil {
		return nil, nil, err
	}

	mobianStgPkgList.ForEach(func(pkg *deb.Package) error {
		if !hasPkg(masterList, pkg.Name) {
			newPkg := mobianPkg{
				name:     pkg.Name,
				mTrixie:  "-",
				mStaging: pkg.Version,
			}
			masterList = append(masterList, newPkg)
		} else {
			for i, elem := range masterList {
				if strings.Compare(elem.name, pkg.Name) == 0 {
					myPkg := &masterList[i]
					myPkg.mStaging = pkg.Version
					break
				}
			}
		}
		return nil
	})

	debianTrxPkgList, err := buildRepo("debian", "https://deb.debian.org/debian", "testing", component, db)
	if err != nil {
		return nil, nil, err
	}

	debianUnsPkgList, err := buildRepo("debian", "https://deb.debian.org/debian", "unstable", component, db)
	if err != nil {
		return nil, nil, err
	}

	for i, elem := range masterList {
		myPkg := &masterList[i]
		myPkg.dTesting = getDebianVersion(debianTrxPkgList, elem.name)
		myPkg.dUnstable = getDebianVersion(debianUnsPkgList, elem.name)
		myPkg.upstream = getUpstreamVersion(elem.name)
	}

	sort.Slice(masterList, func(i, j int) bool {
		return strings.Compare(masterList[i].name, masterList[j].name) < 0
	})

	return stableList, masterList, nil
}

func main() {
	db, err := goleveldb.NewOpenDB("/tmp/listpkg/repo.db")
	if err != nil {
		log.Fatalf("%+v\n", err)
	}

	jsonfile, err := os.ReadFile("/var/www/packages/.upstream-versions.json")
	if err != nil {
		log.Fatalln("Couldn't open the JSON file", err)
	}

	// Parse the file
	err = json.Unmarshal(jsonfile, &upstreamJSON)
	if err != nil {
		log.Fatalln("Unable to read upstream versions", err)
	}

	mainStableList, mainList, err := populateMasterList("main", db)
	if err != nil {
		log.Fatalln("Unable to read main packages lists", err)
	}

	nonFreeStableList, nonFreeList, err := populateMasterList("non-free-firmware", db)
	if err != nil {
		log.Fatalln("Unable to read non-free-firmware packages lists", err)
	}

	createFile(os.Args[1], mainList, nonFreeList, mainStableList, nonFreeStableList)
}
